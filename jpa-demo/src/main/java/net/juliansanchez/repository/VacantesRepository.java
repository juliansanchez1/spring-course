package net.juliansanchez.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import net.juliansanchez.model.Vacante;

public interface VacantesRepository extends JpaRepository<Vacante, Integer> {
	
	List<Vacante> findByEstatus(String estatus);
	
	List<Vacante> findByDestacadoAndEstatusOrderByIdDesc(int destacado, String estatus);
	
	List<Vacante> findBySalarioBetweenOrderBySalarioDesc(double salario1, double salario2);
	
	List<Vacante> findByEstatusIn(String[] estatus);

}
