package net.juliansanchez.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.juliansanchez.model.Categoria;

//public interface CategoriasRepository extends CrudRepository<Categoria, Integer> {
public interface CategoriasRepository extends JpaRepository<Categoria, Integer> {

}
