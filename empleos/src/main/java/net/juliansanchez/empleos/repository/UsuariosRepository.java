package net.juliansanchez.empleos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.juliansanchez.empleos.model.Usuario;

public interface UsuariosRepository extends JpaRepository<Usuario, Integer> { 
	Usuario findByUsername(String username);
}
