package net.juliansanchez.empleos.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import net.juliansanchez.empleos.model.Perfil;

public interface PerfilesRepository extends JpaRepository<Perfil, Integer> {

}
