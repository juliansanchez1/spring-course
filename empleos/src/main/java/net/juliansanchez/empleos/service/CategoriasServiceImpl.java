package net.juliansanchez.empleos.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import net.juliansanchez.empleos.model.Categoria;

@Service
public class CategoriasServiceImpl implements ICategoriasService{
	List<Categoria> listaCategorias = null;
	
	public CategoriasServiceImpl() {
		
		listaCategorias = new LinkedList<Categoria>();
		
		// Creamos algunas Categorias para poblar la lista ...
		
		// Categoria 1
		Categoria cat1 = new Categoria();
		cat1.setId(1);
		cat1.setNombre("Contabilidad");
		cat1.setDescripcion("Descripcion de la categoria Contabilidad");
		
		// Categoria 2
		Categoria cat2 = new Categoria();
		cat2.setId(2);
		cat2.setNombre("Ventas");
		cat2.setDescripcion("Trabajos relacionados con Ventas");
		
					
		// Categoria 3
		Categoria cat3 = new Categoria();
		cat3.setId(3);
		cat3.setNombre("Comunicaciones");
		cat3.setDescripcion("Trabajos relacionados con Comunicaciones");
		
		// Categoria 4
		Categoria cat4 = new Categoria();
		cat4.setId(4);
		cat4.setNombre("Arquitectura");
		cat4.setDescripcion("Trabajos de Arquitectura");
		
		// Categoria 5
		Categoria cat5 = new Categoria();
		cat5.setId(5);
		cat5.setNombre("Educacion");
		cat5.setDescripcion("Maestros, tutores, etc");
		
		// Categoria 6
		Categoria cat6 = new Categoria();
		cat6.setId(6);
		cat6.setNombre("Desarrollo de Software");
		cat6.setDescripcion("Trabajo para programadores");
		
		/**
		 * Agregamos los 5 objetos de tipo Categoria a la lista ...
		 */
		listaCategorias.add(cat1);			
		listaCategorias.add(cat2);
		listaCategorias.add(cat3);
		listaCategorias.add(cat4);
		listaCategorias.add(cat5);
		listaCategorias.add(cat6);

	}
	
	public void guardar(Categoria categoria) {
		listaCategorias.add(categoria);
		
	}

	public List<Categoria> buscarTodas() {
		return listaCategorias;
	}

	public Categoria buscarPorId(Integer idCategoria) {
		for (Categoria c : listaCategorias) {
			if (c.getId() == idCategoria) {
				return c;
			}
		}
		
		return null;
	}

	@Override
	public void eliminar(Integer idCategoria) {
		// TODO Auto-generated method stub
		
	}

}
