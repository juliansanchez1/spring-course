package net.juliansanchez.empleos.service;

import java.util.List;

import net.juliansanchez.empleos.model.Usuario;

public interface IUsuariosService {

	void guardar(Usuario usuario);
	void eliminar(Integer idUsuario);
	List<Usuario> buscarTodos();
	Usuario buscarPorUsername(String username);
}


