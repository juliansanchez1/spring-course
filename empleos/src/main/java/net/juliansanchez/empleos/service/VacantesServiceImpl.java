package net.juliansanchez.empleos.service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.juliansanchez.empleos.model.Vacante;

@Service
public class VacantesServiceImpl implements IVacantesService {
	private List<Vacante> listaVacantes = null;
	
	public VacantesServiceImpl() {
		listaVacantes = new LinkedList<Vacante>();
		
		try {
			String nombre = "Ingeniero Civil";
			String desc = "Solicitamos Ing. Civil para diseñar puente peatonal.";
			LocalDate fecha = LocalDate.of(2019,02,8);
			double salario = 8500.0;
			
			Vacante vacante1 = new Vacante();
			vacante1.setId(1);
			vacante1.setNombre(nombre);
			vacante1.setDescripcion(desc);
			vacante1.setFecha(fecha);
			vacante1.setSalario(salario);		
			vacante1.setDestacado(1);
			vacante1.setImagen("empresa1.png");
			
			nombre = "Contador Público";
			desc = "Empresa importante solicita contador con 5 años de experiencia titulado.";
			fecha = LocalDate.of(2019,02,9);
			salario = 12000.0;
			
			Vacante vacante2 = new Vacante();
			vacante2.setId(2);
			vacante2.setNombre(nombre);
			vacante2.setDescripcion(desc);
			vacante2.setFecha(fecha);
			vacante2.setSalario(salario);	
			vacante2.setDestacado(0);
			vacante2.setImagen("empresa2.png");
			
			nombre = "Ingeniero de Eléctrico";
			desc = "Empresa internacional solicita ingeniero eléctrico para mantenimiento de la instalación eléctrica.";
			fecha = LocalDate.of(2019,02,10);
			salario = 10500.0;
			
			Vacante vacante3 = new Vacante();
			vacante3.setId(3);
			vacante3.setNombre(nombre);
			vacante3.setDescripcion(desc);
			vacante3.setFecha(fecha);
			vacante2.setSalario(salario);	
			vacante3.setDestacado(0);
			
			nombre = "Diseñador Gráfico";
			desc = "Solicitamos Diseñador Gráfico titulado para diseñar publicidad de la empresa.";
			fecha = LocalDate.of(2019,02,11);
			salario = 7500.0;
			
			Vacante vacante4 = new Vacante();
			vacante4.setId(4);
			vacante4.setNombre(nombre);
			vacante4.setDescripcion(desc);
			vacante4.setFecha(fecha);
			vacante4.setSalario(salario);	
			vacante4.setDestacado(1);
			vacante4.setImagen("empresa3.png");
			listaVacantes.add(vacante1);
			listaVacantes.add(vacante2);
			listaVacantes.add(vacante3);
			listaVacantes.add(vacante4);
		}catch (Exception e) {
			System.out.println("Error:" + e.getMessage());
		}
		
	}
	
	public List<Vacante> buscarTodas() {
		return listaVacantes;
	}

	public Vacante buscarPorId(int idVacante) {
		for (Vacante v : listaVacantes) {
			if (v.getId() == idVacante) {
				return v;
			}
		}
		
		return null;
	}

	public void guardar(Vacante vacante) {
		listaVacantes.add(vacante);
	}

	@Override
	public List<Vacante> buscarDestacadas() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void eliminar(Integer idVacante) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Vacante> buscarByExample(Example<Vacante> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Vacante> buscarTodas(Pageable page) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
